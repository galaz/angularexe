import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {

  // Adding emitters
  @Output() addUser:EventEmitter <any> = new EventEmitter <any>();
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>();

  // Instance Variables
  id;
  user;
  service:UsersService;

  //Form Builder
  userForm = new FormGroup({
      name:new FormControl(),
      phone:new FormControl(),
      id:new FormControl()
  });  

  constructor(service:UsersService, private formBuilder:FormBuilder, private route: ActivatedRoute) {   	    
    this.service = service;    
    
  }

  sendData(){
    this.addUser.emit(this.userForm.value.name);
    this.userForm.value.id = this.id;
    
    this.service.updateUser(this.userForm.value).subscribe(
      response =>{              
        this.addUserPs.emit();
      }

    )
  }

	ngOnInit() { 
    this.route.paramMap.subscribe(params => {
      this.id = +params.get('id'); // This line converts id from string into num      
       this.service.getUser(this.id).subscribe(response=>{
        this.user = response.json();                                
      });      
    });       	  
  }

}
