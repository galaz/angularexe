import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UsersService {
// Variables
  http:Http;

  getUsers(){  
	return this.http.get('http://localhost:8020/slim/users');
  }

  postUser(data){
        let params = new HttpParams().append('name',data.name).append('phone',data.phone);      
        let options =  {
          headers:new Headers({
            'content-type':'application/x-www-form-urlencoded'    
          })
        }      
        return this.http.post('http://localhost:8020/slim/users', params.toString(), options);
      }

          deleteUser(key){      
            return this.http.delete('http://localhost:8020/slim/users/delete/'+key);
          }
        
          getUser(key){  
             return this.http.get('http://localhost:8020/slim/users/'+key);
          }
        
          updateUser(data){       
            let options = {
              headers: new Headers({
                'content-type':'application/x-www-form-urlencoded'
              })   
            };
        
            let params = new HttpParams().append('name',data.name).append('phone',data.phone);  
        
            return this.http.put('http://localhost:8020/slim/users/'+ data.id,params.toString(), options);      
          }

          getUsersFireBase(){
                return this.db.list('/Users').valueChanges();
              }
            

  constructor(http:Http, private db:AngularFireDatabase) { 
 	this.http = http; 
  }
}
