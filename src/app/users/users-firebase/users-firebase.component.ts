import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-users-firebase',
  templateUrl: './users-firebase.component.html',
  styleUrls: ['./users-firebase.component.css']
})
export class UsersFirebaseComponent implements OnInit {

  // Variables	  
   users;
   usersKeys = [];
   constructor(private service:UsersService) {  	
      	service.getUsersFireBase().subscribe(response=>{       		
        	this.usersKeys = Object.values(response);    	
     	  });
      }
  ngOnInit() {
  }

}
