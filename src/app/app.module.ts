import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersService } from './users/users.service';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UpdateFormComponent } from './users/update-form/update-form.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { UsersFirebaseComponent } from './users/users-firebase/users-firebase.component';
import { UsersNavigationComponent } from './users/users-navigation/users-navigation.component';

const appRoutes: Routes = [
    { path: ''            ,component:UsersComponent},
    { path: 'users'       , component: UsersComponent },
    { path: 'products'    , component: ProductsComponent },
    { path: 'update-form/:id', component: UpdateFormComponent },
    { path: 'users-firebase'      , component: UsersFirebaseComponent },
    { path: '**'          , component: NotFoundComponent }  
  ];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersFormComponent,
    UpdateFormComponent,
    UsersNavigationComponent,
    UsersFirebaseComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot(      
    appRoutes,
    { enableTracing: false })
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
